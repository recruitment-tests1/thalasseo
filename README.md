# Bee Game

## Getting started

### Requirements

- Linux
- Docker 23.0.1
- Docker-compose 1.27.4

### Installation

```
    make setup
```

### Launch tests
```
    make tests
```

### Play
```
    make hit
```