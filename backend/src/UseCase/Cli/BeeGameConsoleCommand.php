<?php

declare(strict_types=1);

namespace App\UseCase\Cli;

use App\Core\Application\HitServiceInterface;
use App\Core\Domain\Queen;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class BeeGameConsoleCommand extends Command
{
    private const CONSOLE_COMMAND_NAME = 'game:bee:hit';
    private const CONSOLE_COMMAND_DESCRIPTION = 'game:bee:hit';

    public function __construct(
        private readonly HitServiceInterface $hitService,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::CONSOLE_COMMAND_NAME)
            ->setDescription(self::CONSOLE_COMMAND_DESCRIPTION);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('<info>Looking for bee to hit.</>');
        $strickenBee = $this->hitService->hit();

        $output->writeln(sprintf(
            '<info>Bee number %s has been hit. He only has %s hp left.</>',
            $strickenBee->getId(),
            $strickenBee->getHitPoints(),
        ));

        if ($strickenBee instanceof Queen && $strickenBee->getHitPoints() === 0) {
            $output->writeln('<info>Game over ! The queen is dead !</>');
        }

        return Command::SUCCESS;
    }
}