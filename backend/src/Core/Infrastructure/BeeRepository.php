<?php

declare(strict_types=1);

namespace App\Core\Infrastructure;

use App\Core\Application\BeeInitializeRepositoryInterface;
use App\Core\Application\HitBeeRepositoryInterface;
use App\Core\Domain\BeeInterface;
use App\Core\Domain\BeeRoleEnum;
use App\Core\Infrastructure\Entity\Bee;
use Doctrine\ORM\EntityManagerInterface;

final readonly class BeeRepository implements BeeInitializeRepositoryInterface, HitBeeRepositoryInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {
    }

    public function killAllBees(): void
    {
        $this->entityManager->createQueryBuilder()
            ->delete(Bee::class, 'b')
            ->getQuery()
            ->execute();
    }

    public function saveBees(BeeInterface ...$bees): void
    {
        foreach ($bees as $bee) {
            $this->entityManager->persist(new Bee(BeeRoleEnum::tryFrom($bee::class), $bee->getHitPoints()));
        }
        $this->entityManager->flush();
    }

    public function getAllBees(): array
    {
        return $this->entityManager->createQueryBuilder()
            ->select('b')
            ->from(Bee::class, 'b')
            ->where('b.hitPoints > 0')
            ->getQuery()
            ->execute();
    }

    public function saveBee(BeeInterface $beeModel, Bee $bee): void
    {
        $this->entityManager->persist($bee->setHitPoints($beeModel->getHitPoints()));
        $this->entityManager->flush();
    }
}