<?php

declare(strict_types=1);

namespace App\Core\Infrastructure\Entity;

use App\Core\Domain\BeeRoleEnum;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'Bee')]
class Bee
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue(strategy: 'AUTO')]
    private int $id;

    #[ORM\Column(type: 'string')]
    private string $role;

    #[ORM\Column(type: 'integer')]
    private int $hitPoints;

    public function __construct(BeeRoleEnum $role, int $hitPoints)
    {
        $this->role = $role->value;
        $this->hitPoints = $hitPoints;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getRole(): BeeRoleEnum
    {
        return BeeRoleEnum::from($this->role);
    }

    public function getHitPoints(): int
    {
        return $this->hitPoints;
    }

    public function setHitPoints(int $hitPoints): self
    {
        $this->hitPoints = $hitPoints;

        return $this;
    }
}