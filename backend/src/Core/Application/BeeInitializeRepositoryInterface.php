<?php

declare(strict_types=1);

namespace App\Core\Application;

use App\Core\Domain\BeeInterface;

interface BeeInitializeRepositoryInterface
{
    public function killAllBees(): void;

    public function saveBees(BeeInterface ...$bees): void;
}