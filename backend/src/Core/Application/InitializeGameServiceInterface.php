<?php

declare(strict_types=1);

namespace App\Core\Application;

interface InitializeGameServiceInterface
{
    public function start(): void;
}