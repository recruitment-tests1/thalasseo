<?php

declare(strict_types=1);

namespace App\Core\Application;

use App\Core\Domain\BeeInterface;
use App\Core\Infrastructure\Entity\Bee;

interface HitBeeRepositoryInterface
{
    /** @return Bee[] */
    public function getAllBees(): array;

    public function saveBee(BeeInterface $beeModel, Bee $bee): void;
}