<?php

declare(strict_types=1);

namespace App\Core\Application;


use App\Core\Domain\BeeInterface;

interface HitServiceInterface
{
    public function hit(): BeeInterface;
}