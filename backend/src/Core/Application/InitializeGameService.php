<?php

declare(strict_types=1);

namespace App\Core\Application;

use App\Core\Domain\BeeInterface;
use App\Core\Domain\Queen;
use App\Core\Domain\Scout;
use App\Core\Domain\Worker;

final readonly class InitializeGameService implements InitializeGameServiceInterface
{
    private const NUMBER_OF_QUEENS = 1;
    private const NUMBER_OF_WORKERS = 5;
    private const NUMBER_OF_SCOUTS = 8;

    public function __construct(
        private BeeInitializeRepositoryInterface $beeInitializeRepository,
    ) {
    }

    public function start(): void
    {
        $this->beeInitializeRepository->killAllBees();
        $this->beeInitializeRepository->saveBees(
            ...$this->generateQueen(),
            ...$this->generateWorkers(),
            ...$this->generateScouts(),
        );
    }

    /** @return BeeInterface[] */
    private function generateQueen(): array
    {
        $bees = [];
        for ($i = 0; $i < self::NUMBER_OF_QUEENS; $i++) {
            $bees[] = new Queen();
        }

        return $bees;
    }

    /** @return BeeInterface[] */
    private function generateWorkers(): array
    {
        $bees = [];
        for ($i = 0; $i < self::NUMBER_OF_WORKERS; $i++) {
            $bees[] = new Worker();
        }

        return $bees;
    }

    /** @return BeeInterface[] */
    private function generateScouts(): array
    {
        $bees = [];
        for ($i = 0; $i < self::NUMBER_OF_SCOUTS; $i++) {
            $bees[] = new Scout();
        }

        return $bees;
    }
}