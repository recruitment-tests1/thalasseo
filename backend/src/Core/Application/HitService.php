<?php

declare(strict_types=1);

namespace App\Core\Application;

use App\Core\Domain\BeeInterface;
use App\Core\Domain\BeeRoleEnum;
use App\Core\Domain\Queen;
use App\Core\Domain\Scout;
use App\Core\Domain\Worker;
use App\Core\Infrastructure\Entity\Bee;

final readonly class HitService implements HitServiceInterface
{
    public function __construct(
        private HitBeeRepositoryInterface $hitBeeRepository,
        private InitializeGameServiceInterface $initializeGameService,
    ) {
    }

    public function hit(): BeeInterface
    {
        $bees = $this->hitBeeRepository->getAllBees();
        if (count($bees) === 0) {
            $this->initializeGameService->start();
            $bees = $this->hitBeeRepository->getAllBees();
        }

        $selectedBee = $bees[array_rand($bees)];

        $bee = $this->getModelFromEntity($selectedBee);
        $bee->hit();

        $this->hitBeeRepository->saveBee($bee, $selectedBee);
        
        return $bee;
    }

    private function getModelFromEntity(Bee $bee): BeeInterface
    {
        return match ($bee->getRole()) {
            BeeRoleEnum::QUEEN => (new Queen($bee->getHitPoints(), $bee->getId())),
            BeeRoleEnum::WORKER => (new Worker($bee->getHitPoints(), $bee->getId())),
            BeeRoleEnum::SCOUT => (new Scout($bee->getHitPoints(), $bee->getId())),
        };
    }
}