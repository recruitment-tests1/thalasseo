<?php

declare(strict_types=1);

namespace App\Core\Domain;

final class Queen extends AbstractBee implements BeeInterface
{
    private const HIT_POINTS = 100;
    private const DAMAGE_TAKEN = 15;

    public function __construct(
        ?int $hitPoints = null,
        ?int $id = null,
    ) {
        parent::__construct($hitPoints ?? self::HIT_POINTS, $id);
    }

    public function hit(): BeeStateEnum
    {
        return $this->genericHit(self::DAMAGE_TAKEN);
    }
}