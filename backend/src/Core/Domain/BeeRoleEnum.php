<?php

namespace App\Core\Domain;

enum BeeRoleEnum: string
{
    case QUEEN = Queen::class;
    case WORKER = Worker::class;
    case SCOUT = Scout::class;
}
