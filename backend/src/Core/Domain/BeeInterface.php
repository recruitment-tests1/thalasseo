<?php

declare(strict_types=1);

namespace App\Core\Domain;

interface BeeInterface
{
    public function hit(): BeeStateEnum;

    public function getHitPoints(): int;

    public function getId(): int;
}