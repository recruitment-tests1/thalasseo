<?php

namespace App\Core\Domain;

enum BeeStateEnum
{
    case ALIVE;
    case DEAD;
}
