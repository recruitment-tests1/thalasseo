<?php

declare(strict_types=1);

namespace App\Core\Domain;

use DomainException;

abstract class AbstractBee
{
    protected BeeStateEnum $state;

    public function __construct(
        protected int $hitPoints,
        protected readonly ?int $id = null,
    ) {
        if ($this->hitPoints <= 0) {
            $this->state = BeeStateEnum::DEAD;
            return;
        }

        $this->state = BeeStateEnum::ALIVE;
    }

    protected function genericHit(int $damageTaken): BeeStateEnum
    {
        $this->hitPoints -= $damageTaken;

        if ($this->hitPoints <= 0) {
            $this->hitPoints = 0;
            $this->state = BeeStateEnum::DEAD;
            return $this->state;
        }

        $this->state = BeeStateEnum::ALIVE;
        return $this->state;
    }

    public function getHitPoints(): int
    {
        return $this->hitPoints;
    }

    public function getId(): int
    {
        if ($this->id === null) {
            throw new DomainException('Bee id can\'t be null');
        }

        return $this->id;
    }
}