<?php

declare(strict_types=1);

namespace App\Tests;

use App\Core\Application\BeeInitializeRepositoryInterface;
use App\Core\Application\HitServiceInterface;
use App\Core\Domain\Queen;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class HitServiceIntegrationTest extends KernelTestCase
{
    private readonly HitServiceInterface $hitService;
    private readonly BeeInitializeRepositoryInterface $beeInitializeRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->hitService = self::getContainer()->get(HitServiceInterface::class);
        $this->beeInitializeRepository = self::getContainer()->get(BeeInitializeRepositoryInterface::class);
    }

    /** @test */
    public function it_should(): void
    {
        $this->beeInitializeRepository->killAllBees();
        $this->beeInitializeRepository->saveBees(new Queen());
        for ($i = 0 ; $i < 7 ; $i++) {
            $bee = $this->hitService->hit();
            self::assertEquals(max(100 - (($i + 1) * 15), 0), $bee->getHitPoints());
        }
    }
}