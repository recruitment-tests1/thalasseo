
setup:
	docker-compose up -d
	docker-compose exec php-fpm composer install
	docker-compose exec php-fpm bin/console doc:sch:up --force --no-interaction

tests:
	docker-compose exec php-fpm php bin/phpunit


hit:
	docker-compose exec php-fpm bin/console game:bee:hit
